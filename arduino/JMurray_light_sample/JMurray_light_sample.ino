#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define LED_PIN 11

/* -------- LEDs--------- */
Adafruit_NeoPixel light = Adafruit_NeoPixel(2, LED_PIN, NEO_GRB + NEO_KHZ800); 
uint32_t orange = light.Color(223, 81, 27);
uint32_t blue = light.Color(0, 255, 85);
uint32_t clr = orange;
const int max_bright = 215;
const int min_bright = 8;
int targ_bright = 140;
int cur_bright = 215;
unsigned long light_mil;
int heartbeatArray[] = {-6,-2,1,5,10,15,20,30,42,57,73,90,105,119,131,140,146,149,148,144,137,128,117,106,94,82,71,64,58,54,50,49,49,50,52,54,55,55,55,53,51,48,45,41,38,34,30,27,23,20,18,16,15,14,13,13,12,12,12,13,13,14,14,15,15,14,14,14,13,12,12,12,11,11,10,10,9,8,7,6,5,4,4,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,4,4,4,4,4,4,3,3,3,2,2,2,1,1,2,2,2,3,3,3,4,4,4,4,4,4,4,5,5,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,5,5,5,5,5,6,8,12,17,23,30,38,47,55,61,67,71,73,73,72,70,65,60,54,49,44,39,35,32,31,30,28,26,24,21,19,16,14,12,10,9,9,9,9,10,12,14,15,17,18,20,20,20,20,20,19,18,18,17,17,16,16,16,15,15,14,13,12,11,9,9,8,8,9,9,10,11,11,12,11,11,11,10,9,8,7,7,6,6,6,5,5,5,5};
int numBeats = sizeof(heartbeatArray)/sizeof(int);
int beatDelay = 1000;
static double lightMult = 0;

void setup() {

  Serial.begin(115200);

  // Initialize light
  light.begin();
  light.setBrightness(min_bright);
  light.setPixelColor(0, clr); // reset color - it could change too
  light.setPixelColor(1, clr); // reset color - it could change too
  light.show(); // off

  // set timers
  light_mil = millis();


}

void loop() {
   
   runLights();
   Trace();

}

void Trace() {
  static unsigned long trace_tm;
  static unsigned int trace_every = 1000;
  if ( (long)(millis() - trace_tm) >= 0 )
  {
    Serial.print("cur_bright: ");
    Serial.println(cur_bright);
    Serial.println(sizeof(heartbeatArray)/sizeof(int));
    Serial.println(numBeats);
    Serial.println(lightMult);
    trace_tm = millis() + trace_every;
  }
}

void runLights() {

  static unsigned long light_tm;
  static unsigned int light_every = 6;
  static int curBeat = 0;
  
  if ( (long)(millis() - light_tm) >= 0 )
  {
    //cur_bright = ((cur_bright == max_bright) ? min_bright : max_bright);
    //clr = ((clr == blue) ? orange : blue);
    clr = light.Color(139,0, 0);
    light_tm = millis() + light_every;  
    lightMult = (double)heartbeatArray[curBeat]/(double)1.6;
    if (lightMult < 7) lightMult = random(1,3);
      cur_bright = (int)lightMult;
      light.setBrightness(cur_bright);
      light.show();
      curBeat += 3;
      if (curBeat >=numBeats) {
        light_tm = millis() + 600;
        curBeat=0;
      }
  }
  
  /* THIS IS AN EASE-TO-TARGET_BRIGHTNESS test I was running. 
  targ_bright = map((int)dist, MIN_DISTANCE, MAX_DISTANCE, max_bright, min_bright);
  cur_bright += ((targ_bright - cur_bright) / 4);
  light.setBrightness(cur_bright);
  */
  
}


