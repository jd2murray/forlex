# calculates the
import matplotlib.pyplot as plt
from scipy.fftpack import fft
from scipy.signal import savgol_filter
from pylab import*
from scipy.io import wavfile
from statsmodels.nonparametric.smoothers_lowess import lowess
import numpy as np

sampFreq, snd = wavfile.read('heartbeatsmall8000.wav')
if snd.dtype == 'int16':
    #snd = snd / (2. ** 15)
    snd = snd / (100.)

#work with one channel
s1 = snd[0:,0]

#array of rms averages windows
windowSize = 100
s1Sqr = np.power(s1, 2)
window = np.ones(windowSize)/float(windowSize)
s1RMS = np.sqrt(np.convolve(s1Sqr, window, mode='same'))
stepSize = 10
s1RMS = s1RMS[::stepSize]

s1 = s1[::stepSize]
#plot the waveform of the single channel
timeArray = np.arange(0, s1.size, 1)
timeArray = timeArray / sampFreq
timeArray = timeArray * 1000
filtered = lowess(s1RMS, timeArray, is_sorted=True, frac=0.05, it=0, missing='drop')

plt.figure(1)
plt.subplot(311)
plt.plot(np.arange(0,s1.size,1), s1, color='k')
ylabel('Amplitude')
xlabel('Time (ms)')
plt.subplot(312)
plt.plot(timeArray, s1RMS, color='k')
plt.subplot(313)
plt.plot(filtered[:,0], (filtered[:,1]), color='k')

#save the filtered array
outputArray = filtered[:,1].astype(int)
f = open('data.c', 'w')
f.write('int sampFreq = ' + sampFreq.__str__() + ';\n')
f.write('double max = ' + outputArray.max().__str__() + ';\n')
f.write('double min = ' + outputArray.min().__str__() + ';\n')
f.write('int heartbeatArray = {')
for o in outputArray:
    f.write(o.__str__() + ',')

f.close()


print('hello')
