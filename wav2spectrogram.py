#plots a spectrogram for a wave file
#hardcoded bitdepth, frequency etc.

import matplotlib.pyplot as plt
from scipy.fftpack import fft
from scipy.io import wavfile

fs, data = wavfile.read('heartbeatsmall.wav')
a = data.T[1]  # this is a two channel soundtrack, I get the first track
b = [(ele / 2 ** 16.) * 2 for ele in a]  # this is 8-bit track, b is now normalized on [-1,1)

NFFT = 400       # the length of the windowing segments
Fs = 8000  # the sampling frequency
scale = 'linear'
mode = 'psd'

Pxx, freqs, bins, im = plt.specgram(b, NFFT=NFFT, Fs=Fs, noverlap=350, scale=scale, mode=mode)
plt.show()

#plt.plot(b[:len(b)], 'r')
#c = fft(b)  # calculate fourier transform (complex numbers list)
#d = len(c) / 2  # you only need half of the fft list (real signal symmetry)
#plt.plot(abs(c[:int(d - 1)]), 'r')
#plt.plot(c[:int(d - 1)], 'r')
#plt.show()

print('hello')
